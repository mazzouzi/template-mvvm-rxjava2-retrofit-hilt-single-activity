package com.mazapps.template.data.model

data class Continue(
    val `continue`: String,
    val sroffset: Int
)