package com.mazapps.template.data

import com.mazapps.template.data.model.Wikipedia
import com.mazapps.template.data.network.ApiHelper
import io.reactivex.Single
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author morad.azzouzi on 01/12/2019.
 */
@Singleton
open class AppDataManager @Inject constructor(
    private val apiHelper: ApiHelper
) : DataManager {

    override fun getCount(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Single<Response<Wikipedia>> = apiHelper.getCount(action, format, list, keyword)
}