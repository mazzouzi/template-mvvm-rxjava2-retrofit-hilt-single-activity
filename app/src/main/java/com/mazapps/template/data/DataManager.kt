package com.mazapps.template.data

import com.mazapps.template.data.network.ApiHelper

/**
 * @author morad.azzouzi on 11/11/2020.
 */
interface DataManager : ApiHelper