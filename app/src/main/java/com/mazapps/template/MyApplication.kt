package com.mazapps.template

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * @author morad.azzouzi on 11/11/2020.
 */

/**
 * This generated Hilt component is attached to the Application object's lifecycle and
 * provides dependencies to it. Additionally, it is the parent component of the app,
 * which means that other components can access the dependencies that it provides.
 */
@HiltAndroidApp
class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        // do nothing
    }
}