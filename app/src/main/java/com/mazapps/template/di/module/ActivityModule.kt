package com.mazapps.template.di.module

import android.content.Context
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.mazapps.template.data.DataManager
import com.mazapps.template.ui.main.viewmodel.MainViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ActivityContext
import io.reactivex.disposables.CompositeDisposable

/**
 * @author morad.azzouzi on 11/11/2020.
 */
@Module
@InstallIn(ActivityComponent::class)
class ActivityModule {

    @Provides
    fun provideItemDecorator(
        @ActivityContext context: Context
    ) = DividerItemDecoration(context, RecyclerView.VERTICAL)

    @Provides
    fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    fun provideMainViewModel(dataManager: DataManager): MainViewModel = MainViewModel(dataManager)
}