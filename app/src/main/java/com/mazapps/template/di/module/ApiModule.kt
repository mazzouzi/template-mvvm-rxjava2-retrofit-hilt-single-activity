package com.mazapps.template.di.module

import com.mazapps.template.data.AppDataManager
import com.mazapps.template.data.DataManager
import com.mazapps.template.data.network.ApiHelper
import com.mazapps.template.data.network.AppApiHelper
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

/**
 * @author morad.azzouzi on 12/11/2020.
 */
@Module
@InstallIn(ApplicationComponent::class)
abstract class ApiModule {

    /**
     * The @Binds annotation tells Hilt which implementation to use when it needs to provide
     * an instance of an interface.
     */
    @Binds
    @Singleton
    abstract fun bindDataManager(appDataManager: AppDataManager): DataManager

    @Binds
    @Singleton
    abstract fun bindApiHelper(appApiHelper: AppApiHelper): ApiHelper
}