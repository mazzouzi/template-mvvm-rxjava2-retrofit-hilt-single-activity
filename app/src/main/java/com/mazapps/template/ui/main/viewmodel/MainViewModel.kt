package com.mazapps.template.ui.main.viewmodel

import com.mazapps.template.data.CallStateEnum
import com.mazapps.template.data.DataManager
import com.mazapps.template.data.model.Wikipedia
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import retrofit2.Response

/**
 * @author morad.azzouzi on 11/11/2020.
 */
class MainViewModel(private val dataManager: DataManager) {

    val state: BehaviorSubject<CallStateEnum> = BehaviorSubject.createDefault(CallStateEnum.IDLE)
    var count: Int? = null

    fun getKeywordCount(): Disposable {
        state.onNext(CallStateEnum.IN_PROGRESS)
        return dataManager
            .getCount("query", "json", "search", KEYWORD)
            .subscribe(
                { handleGetKeyWordCountResponse(it) },
                { handleGetKeyWordCountError() }
            )
    }

    private fun handleGetKeyWordCountResponse(response: Response<Wikipedia>) {
        val body = response.body()
        if (response.isSuccessful && body != null) {
            handleGetKeyWordCountSuccess(body)
        } else {
            handleGetKeyWordCountError()
        }
    }

    private fun handleGetKeyWordCountSuccess(body: Wikipedia) {
        count = body.query.searchinfo.totalhits
        state.onNext(CallStateEnum.SUCCESS)
        state.onNext(CallStateEnum.IDLE)
    }

    private fun handleGetKeyWordCountError() {
        state.onNext(CallStateEnum.ERROR)
        state.onNext(CallStateEnum.IDLE)
    }

    companion object {
        const val KEYWORD = "sasuke"
    }
}